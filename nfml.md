# Not feeling myself, lately    
  
In time she understood that she would never have an ordinary day again, in this ordinary place,  
that the rest of her life would be a struggle with the unfamiliar  
  
<br/>
<br/>

[I liked the way you said my name](/nfml/iltwysmn)
  
[Burning](/nfml/burning) 
  
[Drama King](/nfml/drama)   
  
[Haven't met you yet](/nfml/hmyy)   
  
[Si supieras](/nfml/sisupieras)  
    
<br/>
<br/>
Mix & Master Engineer: MdeMixage  
Producer & Performed: Jack Wint-Reilly  
Written, Performed & Executive production: Marianna Linares  
  
Recorded at Spiritual Records Studio   
  
January 2024