<b>She</b>

She wanted more than she had  
Not any jewels or cash  
She wanted something to feel and to hold and to  
Give all the love in her heart  

She wanted to play her songs  
Maybe touch one lonely soul  
She wanted audiences clapping and singing along  
And to conquer the world  

She thought it would feel real fine  
To hug a man late at night  
Someone who was good and kind and respectful and nice  
someone just to talk  

She wants a countryside home  
Not very big but not very small  
She wants a garden and trees walking barefoot  
Feeling the cold earth on her feet  

She wants flowers in her hair  
A dress that floats whenever she's dancing  
She wants the music to play everyday  
And to read all the books on the shelves  

She wants to feel on her face  
The wild wind blowing taking her away  
And so she frequently leaves by herself  
Coming back just because she wants to play  

She likes her warm innocence  
And so she talks with enviable grace  
Her perfume lingers in rooms  
And her laugh will make everyone laugh with her too...  

Is she asking for too much?  
Not if you knew her not if you know  
How her mind works turning magically every word  
Into song, into song  

She wanted more than she had  
