# About me  
  
1995 - Caracas, Venezuela  
2004 - Girona, Spain  
2017 - London, UK  
  
I like  
flowers and books and moving in slow motion.  
Honesty and riding bikes around London streets.  
Nature, water and a full moon.  
Songwriting and good people.  
Being in the studio battleing with ideas, royalties.  
Singing, tech, music publishing.      
  
Love     

