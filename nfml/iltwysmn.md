<b>I liked the way you said my name</b>  
  
I liked the way you said my name  
The way that it dropped from your lips  
I liked the way you made up words and rhymes with it  
Perfectly written poetry  
  
I liked the way you touched my skin  
The way your hand fell from my chest down to my hips  
I liked the way you made me laugh in bed before falling asleep  
The climax of a symphony  
  
We were always nothing  
But that something felt so good  
Silhouettes and rhythms  
You seduced me, I seduced you  
Falling from the edges  
We were both in the deep end  
Someone came and save you  
And you left me there  
  
I liked pretending you were it  
(I really thought you were the one)  
Talking baby names and weddings and rings  
You lied to me a million lies  
And I believed them 'cause I could  
I was a kid in your playground  
  
We were always nothing  
But that something felt so good  
Silhouettes and rhythms  
If you'd move I'd follow you  
Falling from the edges  
We were both in the deep end  
Someone came and save you  
And you left me there  
  
You always knew that this would end  
I was a puppet in your show  
You controlled all my strings  
The public clapped, I played along  
How did you think that I would feel?  
You never thought I was the one...  
  
We were always nothing  
But that something felt so good  
Silhouettes and rhythms  
If you'd move I'd follow you  
Falling from the edges  
We were both in the deep end  
Someone came and save you  
And you left me there  
  
I'm drowning  
I'm drowning  
I'm drowning  
  
And you left me there  
And you left  