<b>Burning</b>  
  
It's been four months, you and I, round and round, dancing the waltz  
24 hours ago I said I was moving on  
It's yes, then it's no, and I know I've been playing with your bones  
I pulled the trigger on the gun your blood is in my hands  
  
I’m sorry for leading you on  
I’m sorry for faking and lying and being romantic when my heart is numb  
I’m sorry I know it is late  
I'm a renegade  
I don’t deserve anything  
Gravity pulls me down  
I am drawn to your light  
And I’m burning  
burning  
  
If you ever wonder why I disappeared from your life  
It’s because I know you want coins and my wallet is dry  
Yet yours are the sweetest of lips my lips had ever tried  
Oh and we both know I have a weakness for candy and spice. 
  
I’m sorry for leading you on  
I’m sorry for faking and lying and being romantic when my heart is numb  
I’m sorry I know it is late  
I'm a renegade  
I don’t deserve anything  
Gravity pulls me down  
I am drawn to your light  
And I’m burning  
burning  
  
I’m sorry, it was just a game  
I’m sorry, I lose, you win  
Because if I tell you the truth I have always loved you  
I’m sorry for my coward heart  
And you deserve someone who screams to the world  
I love you  
But from me  
I love you  
For no one to hear  
So don’t mind if I disappear  
Because you deserve every thing  