<b>Haven't met you yet</b>  
  
With you I could go rob a car  
Then drive right out of town  
Bonnie and Clide in a cloud  
Sunset bound  
  
'Cause with you I would open my doors  
I would wrestle on floors  
I would open my legs  
I would fold  
  
With you I'd imagine all these things and more  
But I haven’t met you yet  
  
I'd tell you how I live in fear  
List insecurities  
All my reasons for screams, how at night I can’t sleep  
But you’d save me from me  
I won't be afraid you’d leave  
And I’d write all my songs for you  
  
With you I'd imagine all these things and more  
But I haven’t met you yet  
  
We’d live in our chocolate box  
Far from that messy world  
English countryside views  
And a dog  
That’s where you would wrap me around your arms on a cold winter night  
With my head on your chest  
  
With you I'd imagine all these things and more  
But I haven’t met you yet  
  
You’d be the anchor to my boat  
The light on a dark road  
Illuminating my way back home  
My escape and my muse  
Your lover and your cure  
Bittersweet medicine  
For our wide open wounds   
  
With you I’d imagine all these things and more  
With you I can give up my oscar winning role  
With you  
With you  
  
But the sun always sets in the west  
And I haven’t met you yet  
