<b>Drama King</b>  
  
I bet you like stories with happy endings  
You'd love for me to come back  
The curtains open and there we are standing  
All dressed up acting our parts  

I'm the sweet doll dancing in all directions  
You bow and arrow in hand  
But I see through your evil machinations  
I leave the stage, final act  
  
You and I  
We are done  
I don't want you to know  
That you broke my heart like no one has  
You so the drama king  
I was wasting my time  
So I learnt how to bite  
And you still have the mark on your arm  
  
I know you like being protagonistic  
Porcelain gestures you used to call love  
I don't want to be part of your orchestrations... anymore  
  
You and I  
We are done  
I don't want you to know  
That you broke my heart like no one has  
You so the drama king  
I was wasting my time  
So I learnt how to bite  
And you still have the mark on your arm  
  
You betrayed my trust, how will I trust again?  
The daemons in your bones woke up all the daemons in mine  
I'm scared  
I may do this to him...  
  
I bet you like stories with happy endings  
You'd love for me to come back (ahah)  
