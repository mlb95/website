# Tell the moon I'm home: on the journey of becoming.  
  
Released sometime in October 2021  
  
The moon, symbolically, represents the feminine. Change, creativity and ladylike energies.  
It's also a symbol of youth and time: both the lunar month and the menstrual cycle run on the same period.  
It is often used as a symbol of wild, uncontrollable forces and,  
in classical antiquity, the greeks, the romans and the aztecs associated the moon with female deities.  
Finally: the man in the moon. The face at many different angles. Represents the ability to look beyond the obvious.  
A gentle reminder from above to keep on seeing things for what they might be as well as what they are. 


[before sunrise, 1995](/ttmih/prelude)  

[blooming](/ttmih/blooming)  

[if you were here](/ttmih/iywh)  

[lady](/ttmih/lady)  

[poison](/ttmih/poison)  

[a woman's body](/ttmih/awb)  

[if you don't mind](/ttmih/iydm)  

[her](/ttmih/her) 
 
[diamonds](/ttmih/diamonds)
