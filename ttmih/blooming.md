# blooming

It seems like spring  
is never the season for me  
I'm finally starting to bloom  
in the summer  

it do is true  
I've never wanted to be just like the rest  
and so it appears I always arrive late  
to the party

I don't miss you  
or that's what I tell myself over and over  
I'm finally writing again  
and it's because you're gone  

August is always the month

I start blooming again  
bring me some flowers and rain  
sisters and brothers and mothers and daughters  
all gathered around on the land  

I've never felt so alive  
when watching the sunrise I started to cry  
the lessons all learnt in the past  
and the music remains  
the music always remains

drama boy  
you force me to leave you when you needed most  
a kiss on the forhead and cuddles  
from sunset to dawn  

I won't pretend  
the hardest decision that I've ever made  
but I'm not a saviour for those who  
don't want to be saved  

August is always the month  

I start blooming again  
bring me some flowers and rain  
sisters and brothers and mothers and daughters  
all gathered around on the land  

I've never felt so alive  
when watching the sunrise I started to cry  
the lessons all learnt in the past  
and the music remains  
the music always remains

It seems like spring is never the season for me  
I'm finally starting to bloom in the summer
