<b>a woman's body</b>

A woman's body is a fairytale  
an eternal sunshine, a day without rain  
is like touching cotton with your fingertips    
like smelling the ocean: smooth, crispy and sweet  

a woman's body is a wonderland  
you'd do almost anything just to make her laugh  
like christmas bells ringing  
like a bee's wing swinging  
like fire flames flicking  
lighting up the sky

a woman's body is a sacred space  
endless curves moving with a gentle grace  
like mountains, like breasts  
like rivers, like hair  
an imagination to fill books and men  

a woman's body keeps the holy grail  
grandmother, a mother, a daughter, a child  
relentless like snow, like an avalanche  
streatching and bringin miracles to life  

whenever she walks, eyes are all on her  
losing your attention when her perfume knocks  
lightly on your door  
playing hide and seek with your thoughts  
nonchalantly but knowing exactly what she's done  

a woman's body is a wonderland  
you'd do almost anything just to make her laugh  
like christmas bells ringing  
like a bee's wing swinging  
like fire flames flicking  
lighting up the sky

