# poison

Are you still thinking about me  
After all these months?  
Oh honey, you should forget me  
I have long moved on  

Only remember the morning  
When you burnt my toast  
And that night when you were flirting  
With, like, a thousand girls

and I know you have a taste for fun  
and for my innocent touch  
just don't call again  
okay?

you were a big mistake  
I should have known all my friends warned me  
you think you can trick me again  
that's cute  
every word you way is poison

you're poison

Are you still sniffing coke?  
because by the looks of it  
you think I'll come back  
only with one call

not even when I'm drunk, my darling  
not even when I'm lonely  
not even if you are the very last man  
on planet earth

and I know you have a taste for fun  
and for my innocent touch  
just don't call again  
are we on the same page?

you were a big mistake  
I should have known all my friends warned me  
you think you can trick me again  
that's cute  
every word you way is poison

you're poison

And I survived  
after I drank it I survived  
and I ain't ever going back  
you're poison

I survived  
turns out I'm smarter than your mind  
who's used to girls running around  
chasing for someone who would love them

I survived  
you see my heart dislikes your heart  
and I ain't ever going back  
you're poison.


