# lady

How does being a lady feel?  
always a dress, once in a while a frown and looking down  
girls should always smile that's how it is  

a lady never says bad words  
if an opinion must be shared, it will, otherwise mouth shut  
look pretty for the evening show

that's what they told us a hundred years ago  
why would you want to silence voices that will and must be heard  
we are not trophies you can't put us on a shelve and forget  
or try to force us to apologise for having a brain, oh no

now you complain everyday about chances we now have  
forgetting how much, for how long, we have been fighting the fight  
against judgmental, pre-established ways of how we should be  
according to the rules we only want to raise families  
but what about raising our voices being inspiring?

I have travelled the world and everywhere is the same  
21st century and still playing the same little game  
of you putting your hands on me because of my smaller frame  
then not believing when I tell the truth  
'she must be crazy'

this is a message for the girls growing up  
not matter where you're from: you're perfect, you have worth and you're loved  
the world is changing, no need to be ashamed or to stand  
no matter what he says if he treats you like shit run fast  
'cause there are boys and men around who are respectful and kind  

Of course a woman can't be sexy and smart  
you'll never find a husband if your ambitions are too high  
why would I want someone I cannot be myself around?  
we can like clothes and books and make-up and be leaders in towns

Don't get me wrong  
we still need a manly hug, a kiss, a dance and some fun  
we don't need you to do everything for us like before  
how ridiculous it sounds feels like I'm telling a joke  
oh no, don't laugh  
there is much work to be done

like, for example, teaching you about how <b>no is no</b>  
how it is not an invitation when I'm wearing a skirt  
how women should support each other, 'cause who will if we don't?  
how we are mothers, we are daughters and we are CEOs

and yes, we have emotions like every human does  
how I'm more vulnerable and stronger than you will ever know  
let's spread the message to the generation now growing up  
'cause changes is happening and it is in our hands if it does

And I want you to know  
that it's your choice: 

If you want to run the world  
or you want to run a home  
or you want to have a job and travel around alone

if you want to have ten kids  
or adopt or none or three  
you're in charge of what you need

it's your choice  
if you want a stable man  
or man's shirt, short skirt and dance (Shania Twain)  
if you want to say 'i love you' to the first who holds your hand

or you have a reserved heart  
and are waiting for the one  
or if you like girls and girls and boys and boys  
no need to judge

oh no need to judge

so, how does being a lady feel?  
hmm hmm 







