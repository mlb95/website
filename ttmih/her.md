# her

I know you weren't for me boy  
I'm not blind  
you said you loved me, you were drunk  
and you lied  
inside my head the question why  
overused  

could I have changed you mind so you'd choose me  
instead of  

Her  
what does she have  
that I don't have  
could be the way she nods at anything you ask  
no petty fights  
an easy flight  
maybe the way she moves her hips or her shy smile  
or plays guitar  
or moans at night  
I'll never know  
and it's okay  
I understand

I know it was me who said no  
many times  
my instinct shouted at me  
"he's not the one"  
yet in my dreams you always called  
asked for a drink, we talk and talk  

could I have changed you mind so you'd choose me  
instead of  

Her  
what does she have  
that I don't have  
could be the way she nods at anything you ask  
no petty fights  
an easy flight  
maybe the way she moves her hips or her shy smile  
or plays the drums  
or moans at night  
I'll never know  
and it's okay  
I understand  

no, no, no  
I don't know why  
you're acting like a kid

I thought you could grow up  
you could give all your love  
to me

so choose

Her  
I'm glad she has  
what I don't have  
that way she nods at every single thing you ask   
no petty fights  
an easy flight  
maybe the way she moves her hips or her shy smile  
or plays guitar  
or moans at night  
I'll never know  
and it's okay  
I understand 
