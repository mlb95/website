# before sunrise, 1995

Oh so you have been on a journey

and you are a stranger to this place

you are an adventurer, a seeker

an adventurer in your mind.

You are interested in the power of the Woman

in a woman's deep strength and creativity

you are becoming this woman.

You need to resign yourself to the awkwardness of life

only if you find peace within yourself you'll find true connection

with others. 

