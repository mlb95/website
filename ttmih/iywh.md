# if you were here

Most days when I wake up  
the morning air, messing my hair  
it takes me back to that October  
when you held my hand under the trees

it takes me back to when I would open my eyes  
surprised, lips curved in smile  
because you were next to me  

Oh if you were here

If you were here  
I'd pinch my arm  
I'd knock on wood  
I'd read the cards  
I would make sure it's not another dream  
like many ones I've had

If you were here  
cuddles in bed  
stolen stares  
and that touch that would make me melt all into you  
I'd make sure you never leave

Most days I can't forget  
the way your presence in a room  
would make the sunny days shine brighter  
and would make the rainy days less blue  

how everytime you offered food  
I would nod and say yes and laugh  
because that's really all I did  
with you

Oh if you were here

If you were here  
I'd pinch my arm  
I'd knock on wood  
I'd read the cards  
I would make sure it's not another dream  
like many ones I've had

If you were here  
cuddles in bed  
stolen stares  
and that touch that would make me melt all into you  
I'd make sure you never leave

Most days I pray to god  
that you remember what you felt and told me  
right before you left, you loved me  
please, at least, make sure my name  
stays  
brings back  
bittersweet memories

If you were here  
I'd pinch my arm  
I'd knock on wood  
I'd read the cards  
I would make sure it's not another dream  
like many ones I've had

If you were here  
cuddles in bed  
stolen stares  
and that touch that would make me melt all into you  
I'd make sure you never leave

