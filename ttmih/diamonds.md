# diamonds

[voice note/first demo]  

I have diamonds  
in the meeting of my thighs  
(Maya Angelou)  

I have diamonds  
right where my heart is  
I have diamonds  
in my,  
in my,  
beating fragile heart

[Instrumental]

I've got diamonds  
in the meeting of my thighs  
I've got diamonds  
in my beating fragile heart  
I've got diamonds  
in the bottom of my shoes  

And I rise  
and I rose  
every time  
that I burn
