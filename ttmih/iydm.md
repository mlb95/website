# if you don't mind

I never really told you but  
I would have loved to build a home with you  

we'd fill the rooms with flowers,   
old guitars, tambourines and pianos too  

I would have loved to tell you about  
how scared I was of opening my heart  

oh and you would tell me:    
"I am no prince charming, but I'll do the best I can"

and man you're doing fine, just fine  
oh let's make this last  
I'll stay by your side, yes I  
will stay if you don't mind  

I never really told you but  
I would have loved to build a family

and have little blonde curls, running around  
and building castles with bed sheets 

I would bite your nose and kiss your lips  
and you would kiss me way below my hips  

and we'd make love every other day  
and bake cakes while we dance to afrobeats

and look!  
we're doing fine, just fine  
oh let's make this last  
I'll stay by your side, yes I  
will stay if you don't mind

and man we're doing fine, just fine  
oh let's make this last  
I'll stay by your side, yes I  
will stay if you don't mind  

I'll stay if you don't mind  
oh, I'll stay if you don't mind

I never really told you but I would have loved to build a home with you  
